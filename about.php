<?php require_once 'header.php'; ?>

<!-- BEGIN OF CONTENT -->
<div id="midbox-container-inner">
    <div id="midbox-inner-bg"></div>
    <div id="midbox-inner">

        <!-- begin of page title -->
        <div id="page-title">
            <h1>About</h1>
        </div>
        <!-- end of page title -->

        <!-- begin of award content -->
        <div id="bread-search-column">
            <div id="breadcumb">                   	
                <span class="bread-img"><img src="images/home-icon.png" alt="" class="bread-img" /></span>
                <span class="bread-txt">&raquo; <a href="#about">About</a> | <a href="#history">History</a> | <a href="#milestones">Milestones</a></span>
            </div>         
        </div>
        <!-- end of award content -->

    </div>

    <div id="content">
        <div class="maincontent">
            <div id="about">
                <h4>About Skyline</h4> 
                <p>PT. Skyline Jaya was officially established on 2005. Operates on 2 main factories with a vast total area of ±10 hectares with approximately 1,000 employees.  Each and every piece of the product is designed, researched and produced at its very own facility. Current annual production output is 800 of 40 feet dry containers, sailed to each continent of the globe.</p> 
                <p>There are in total 10 distributors at the moment (United Kingdom, United States of America, Spain, Rep. Dominica, Mexico, Middle East, Australia, Singapore, Hongkong and Japan) catering for different neighbouring countries off their home base. Skyline design &reg; has also been present at different hospitality projects, both private and commercial, throughout the world.  In addition besides marketing its own label, PT. Skyline Jaya also caters for custom designs suited to our clients request.</p>

                <div class="services-column-vision">
                    <img src="images/vision.jpg" alt="" class="imgleft" />
                    <h4>Our Vision</h4>
                    <p>To become a leader and global player, among the best world class company for long term growth by mastering and developing our people competence</p>

                </div>

                <div class="services-spacer">&nbsp;</div>

                <div class="services-column-mission">
                    <img src="images/mission.jpg" alt="" class="imgleft" />
                    <h4>Our Mission</h4>
                    <p>
                        <b>People  <span style="margin-left:34px">:</span></b> Continuous Improvements in every sector<br/>
                        <b>Punctuality :</b> Customers are assured of prompt production 
                        <span style="margin-left:102px;">and delivery.</span><br/>
                        <b>Product <span style="margin-left:27px">:</span></b> Provide quality products that meets our 
                        <span style="margin-left:104px">customer's needs.</span><br/>
                        <b>Profitability :</b> Maintain synergistic partnerships.<br/>
                    </p>

                </div>
            </div>
            <hr class="content-line" />
            <div id="history">
                <h4>The History</h4> 
                <p>The present status of PT. Skyline Jaya owes to the humble beginning of the establishment of PT. Union  Rattan back in 1986. Major business activities at the time was in natural rattan trading in its basic form  i.e. as raw material, being exported to different countries.</p> 
                <p>A year later lead to the transformation of PT. Balirotan Mas in Surabaya as well as CV. Belanico in Cirebon and in Gorontalo. Both companies involved themselves, apart from raw material trading, into the manufacturing business of the natural rattan into furniture. At this time, the national furniture industry also witnessed the birth of our other sister company located in Kalimantan, namely PT. Pradipta Ratanindo  and CV. Bati Bati Makmur, and then PT. Skyline Jaya was established.</p>
            </div>
            <hr class="content-line" />
            <div id="milestones">
                <h4>Milestones</h4> 
                <ul>
                    <li><b>November 1986  :</b> PT. Union Rattan was established in Surabaya, Jawa Timur.</li>
                    <li><b>August 1987  <span style="margin-left:24px">:</span></b> PT. Union Rattan was transformed into PT. Balirotan Mas residing at its original address in Surabaya
                        <br/> <span style="margin-left:140px">CV. Belanico in Cirebon and in Gorontalo were established</span>
                        <br/> <span style="margin-left:140px">PT. Pradipta Ratanindo and CV. Bati Bati Makmur were established in Banjarmasin.</span></li>                      
                    <li><b>December 2005<span style="margin-left:7px">:</span></b> PT. Skyline Jaya was established following the expansion of business line of outdoor and indoor furniture.</li>
                </ul>  
            </div> 
        </div>  
    </div>

</div>
<!-- END OF CONTENT -->

<?php require_once 'footer.php'; ?>