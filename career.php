<?php require_once 'header.php'; ?>
<!-- BEGIN OF CONTENT -->
<div id="midbox-container-inner">
    <div id="midbox-inner-bg"></div>
    <div id="midbox-inner">

        <!-- begin of page title -->
        <div id="page-title">
            <h1>Career</h1>
        </div>
        <!-- end of page title -->

        <!-- begin of award content -->
        <div id="bread-search-column">
            <div id="breadcumb">                   	
                <span class="bread-img"><img src="images/home-icon.png" alt="" class="bread-img" /></span>
                <span class="bread-txt">&raquo; Career</span>
            </div>           
        </div>
        <!-- end of award content -->

    </div>

    <div id="content">
        <div class="maincontent">

            <p class="italictext">We welcome applications from Responsible individuals who possess a strong sense of Integrity and are constantly striving for <br/>Self-Renewal and Excellence.</p>
            <h4>If you are looking for :</h4>
            <ul class="checklist">
                <li>Greater work exposure</li>
                <li>Opportunities to develop and unleash your full potential</li>
                <li>Warm, friendly and fun-filled working environment</li>                           
                <li>Career development through learning</li>   
                
            </ul>
            <img src="images/handshake.jpg"/><br/>
            Then, BE PART OF US.<br/>
            Send trough your best application to : <a href="mailto:hrd@skylinejaya.id">hrd@skylinejaya.id</a> <br/><br/><br/>
            Warm Regards,<br/>
            HR Department PT. Skyline Jaya<br/> 
        </div>
    </div>

</div>
<!-- END OF CONTENT -->
<?php require_once 'footer.php'; ?>
