<?php require_once 'header.php'; ?>
<link href="box/css/attention_box.css" media="screen" rel="stylesheet" />
<script src="box/js/jquery-1.4.2.min.js"></script>
<script src="box/js/jquery-ui-1.8.1.custom.min.js"></script>
<script src="box/js/attention_box-min.js"></script>
<?php
if (!empty($_POST["submit"])) { 
        $to = 'marketing@skylinejaya.id';
        $subject = '[Website] '. $_POST["subject"];
        $message = 'Nama Pengirim : ' . $_POST["name"]
                . '<br/>Email Pengirim : ' . $_POST["email"]
                . '<br/>-------------------------------------------------------------------' 
                . '<br/>Message' 
                . '<br/>-------------------------------------------------------------------' 
                . '<br/>' . $_POST["message"];
        $message= str_replace(array('<br />','<br/>','<br>'), PHP_EOL, $message);
        mail($to, $subject, $message);

     $to = 'test@skylinejaya.id';
        $subject = '[Website] '. $_POST["subject"];
        $message = 'Nama Pengirim : ' . $_POST["name"]
                . '<br/>Email Pengirim : ' . $_POST["email"]
                . '<br/>-------------------------------------------------------------------' 
                . '<br/>Message' 
                . '<br/>-------------------------------------------------------------------' 
                . '<br/>' . $_POST["message"];
        $message= str_replace(array('<br />','<br/>','<br>'), PHP_EOL, $message);
        mail($to, $subject, $message);
}
?>
<script>
    
    window.onload = init;
<?php
if (!empty($_POST['submit'])) {
    ?>
            function init() {
                AttentionBox.showMessage('Your inquiry has been submitted, we will revert to you as soon as possible, thank you for your keen interest to our Company and Brand.')
            }
    <?
}
?>
</script>
<!-- BEGIN OF CONTENT -->
<div id="midbox-container-inner">
    <div id="midbox-inner-bg"></div>
    <div id="midbox-inner">

        <!-- begin of page title -->
        <div id="page-title">
            <h1>Contact</h1>
        </div>
        <!-- end of page title -->

        <!-- begin of award content -->
        <div id="bread-search-column">
            <div id="breadcumb">                   	
                <span class="bread-img"><img src="images/home-icon.png" alt="" class="bread-img" /></span>
                <span class="bread-txt">&raquo; Contact</span>
            </div>           
        </div>
        <!-- end of award content -->

    </div>

    <div id="content">
        <div class="maincontent">   
            <!-- begin of column left --> 
            <div id="column-support-left">
                <h3>Contact Support</h3>

                <div id="contactFormArea">                                
                    <!-- Contact Form Start //-->
                    <form action="" method="post" id="contactform" /> 
                    <fieldset>
                        <label>Name</label><br />
                        <input type="text" required name="name" class="textfield" id="name" value="" /><br />
                        <label>Email</label><br />
                        <input type="text" required name="email" class="textfield" id="email" value="" /><br />
                        <label>Subject</label><br />
                        <input type="text" required name="subject" class="textfield field-nomargin" id="subject" value="" /><br />
                        <label>Message</label><br />
                        <textarea name="message" id="message" class="textarea" required cols="2" rows="7"></textarea>


                        <input type="submit" name="submit" id="buttonsend" class="input-submit">
                        <span class="loading" style="display: none;">Please wait..</span> 
                    </fieldset>
                    </form>
                    <!-- Contact Form End //-->                                      
                </div>

            </div>
            <!-- end of column left -->

            <!-- begin of column right -->  
            <div id="column-support-right">
                <h3>Additional Resources</h3>
                <img src="images/contact1.jpg" alt="" class="imgleft" />
                <p class="overflow-text"><strong>SKYLINE JAYA - INDOOR FURNITURE PLANT</strong><br />
                    Jalan Margomulyo Indah Kav. 8-10      <br />
                    Surabaya 60186, <br />
                    Jawa Timur - INDONESIA.      <br />
                    Telephone : <img src="images/flag.png" style="width:13px;height: 10px;"/>&nbsp;<a href="callto://+62317490911">(+62)(31) 7490911</a><br />
                    Fax : <img src="images/flag.png" style="width:13px;height: 10px;"/>&nbsp;(+62)(31) 7490908<br /><br />

                </p> 

                <hr />

                <img src="images/contact2.jpg" alt="" class="imgleft" />
                <p class="overflow-text"><strong>SKYLINE JAYA - OUTDOOR FURNITURE PLANT</strong><br />
                    Desa Popoh, Kecamatan Wonoayu      <br />
                    Sidoarjo, Jawa Timur - INDONESIA. <br />
                    Telephone : 
                    <img src="images/flag.png" style="width:13px;height: 10px;"/>&nbsp;<a href="callto://+62318978520">(+62)(31)  8978520 (HUNTING)</a><br />
                    <span style="left:69px;position: relative;"> 
                    </span>
                    Fax : <img src="images/flag.png" style="width:13px;height: 10px;"/>&nbsp;(+62)(31) 8970440<br /><br />
                <b>Email :</b>   <a href="mailto:marketing@skylinejaya.id">marketing@skylinejaya.id</a>
                </p><hr />
                              <img src="images/contact2.jpg" alt="" class="imgleft" />  
                <p class="overflow-text"><strong>SKYLINE DESIGN ASIA PASIFIC MARKETING Pte. Ltd.</strong><br />
                    Office / Showroom : 22, SIN MING LANE #01-74<br />
                    MIDVIEW CITY, SINGAPORE 573969<br />
                    Telephone : 
<a href="callto://+6562620604">+65 6262 0604</a><br /><br />
                    <span style="left:69px;position: relative;"> 
                    </span>
                    Web : <a href="http://www.skylinedesign.es" target="_blank">http://www.skylinedesign.es</a><br />
Web : <a href="http://www.skylinedesign.es" target="_blank">http://www.skylinedesigncontract.es</a><br /><br />
                    
                </p><hr />                   
            </div>
            <!-- end of column right -->                         

        </div>
    </div>

</div>
<!-- END OF CONTENT -->
<?php require_once 'footer.php'; ?>
