<?php require_once 'header.php'; ?>
<link rel="stylesheet" href='css/hoverbox.css' type="text/css" media="screen, projection" />
<!--[if lte IE 7]>
<link rel="stylesheet" href='css/ie_fixes.css' type="text/css" media="screen, projection" />
<![endif]-->

<!-- BEGIN OF CONTENT -->
<div id="midbox-container-inner">
    <div id="midbox-inner-bg"></div>
    <div id="midbox-inner">

        <!-- begin of page title -->
        <div id="page-title">
            <h1>Gallery</h1>
        </div>
        <!-- end of page title -->

        <!-- begin of award content -->
        <div id="bread-search-column">
            <div id="breadcumb">                   	
                <span class="bread-img"><img src="images/home-icon.png" alt="" class="bread-img" /></span>
                <span class="bread-txt">&raquo; Gallery</span>
            </div>    
        </div>
        <!-- end of award content -->

    </div>

    <div id="content">
        <div class="maincontent">   
            <div id="gamKecil">
                <ul class="hoverbox" style="margin-left:40px;"> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery1.jpg"><img src="images/gallery/thumb/gallery1.jpg" alt="description" /><img src="images/gallery/thumb/gallery1.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery2.jpg"><img src="images/gallery/thumb/gallery2.jpg" alt="description" /><img src="images/gallery/thumb/gallery2.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery3.jpg"><img src="images/gallery/thumb/gallery3.jpg" alt="description" /><img src="images/gallery/thumb/gallery3.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery4.jpg"><img src="images/gallery/thumb/gallery4.jpg" alt="description" /><img src="images/gallery/thumb/gallery4.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span>
                    <br/>
                    <span>
                        <li>
                            <a href="images/gallery/gallery5.jpg"><img src="images/gallery/thumb/gallery5.jpg" alt="description" /><img src="images/gallery/thumb/gallery5.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery6.jpg"><img src="images/gallery/thumb/gallery6.jpg" alt="description" /><img src="images/gallery/thumb/gallery6.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery7.jpg"><img src="images/gallery/thumb/gallery7.jpg" alt="description" /><img src="images/gallery/thumb/gallery7.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery8.jpg"><img src="images/gallery/thumb/gallery8.jpg" alt="description" /><img src="images/gallery/thumb/gallery8.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery9.jpg"><img src="images/gallery/thumb/gallery9.jpg" alt="description" /><img src="images/gallery/thumb/gallery9.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery10.jpg"><img src="images/gallery/thumb/gallery10.jpg" alt="description" /><img src="images/gallery/thumb/gallery10.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    
                    <span>
                        <li>
                            <a href="images/gallery/gallery11.jpg"><img src="images/gallery/thumb/gallery11.jpg" alt="description" /><img src="images/gallery/thumb/gallery11.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery12.jpg"><img src="images/gallery/thumb/gallery12.jpg" alt="description" /><img src="images/gallery/thumb/gallery12.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery13.jpg"><img src="images/gallery/thumb/gallery13.jpg" alt="description" /><img src="images/gallery/thumb/gallery13.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery14.jpg"><img src="images/gallery/thumb/gallery14.jpg" alt="description" /><img src="images/gallery/thumb/gallery14.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery15.jpg"><img src="images/gallery/thumb/gallery15.jpg" alt="description" /><img src="images/gallery/thumb/gallery15.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                    <span>
                        <li>
                            <a href="images/gallery/gallery16.jpg"><img src="images/gallery/thumb/gallery16.jpg" alt="description" /><img src="images/gallery/thumb/gallery16.jpg" alt="description" class="preview" /></a>
                        </li>
                    </span> 
                </ul>  
            </div> 
        </div>
    </div>

</div>
<!-- END OF CONTENT -->
<?php require_once 'footer.php'; ?>