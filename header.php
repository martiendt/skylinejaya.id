<!DOCTYPE html>
<html>
    <head> 
        <title>Skyline Jaya</title>

        <!-- ////////////////////////////////// -->
        <!-- //      Start Stylesheets       // -->
        <!-- ////////////////////////////////// -->
        <link href="css/reset.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/noscript.css" rel="stylesheet" type="text/css" media="screen,all" id="noscript" />
        <link href='http://fonts.googleapis.com/css?family=Metrophobic&v1' rel='stylesheet' type='text/css' />

        <!-- ////////////////////////////////// -->
        <!-- //      Javascript Files        // -->
        <!-- ////////////////////////////////// -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.cycle.all.js"></script>
        <script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script>
        <script type="text/javascript" src="js/megamenu.js"></script>

        <!-- ////////////////////////////////// -->
        <!-- //      Space Gallery           // -->
        <!-- ////////////////////////////////// -->
        <link rel="stylesheet" media="screen" type="text/css" href="css/spacegallery/layout.css" />
        <link rel="stylesheet" media="screen" type="text/css" href="css/spacegallery/spacegallery.css" />
        <link rel="stylesheet" media="screen" type="text/css" href="css/spacegallery/custom.css" />
        <script type="text/javascript" src="js/spacegallery/jquery.js"></script>
        <script type="text/javascript" src="js/spacegallery/eye.js"></script>
        <script type="text/javascript" src="js/spacegallery/utils.js"></script>
        <script type="text/javascript" src="js/spacegallery/spacegallery.js"></script>
        <script type="text/javascript" src="js/spacegallery/layout.js"></script>

        
        <!-- //////////////////////////////////// -->
        <!-- //       Scroll To Top            // -->
        <!-- //////////////////////////////////// -->
        <script type="text/javascript" src="js/scrolltopcontrol.js"></script>

        <!-- //////////////////////////////////// -->
        <!-- //          Light Box             // -->
        <!-- //////////////////////////////////// -->
        <link href="css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
        <script type="text/javascript">
            $(function() {

                $('#gamKecil a').lightBox({});

            });
        </script>
        
        <!-- //////////////////////////////////// -->
        <!-- //         Gallery Mac            // -->
        <!-- //////////////////////////////////// --> 
        <script type="text/javascript" src="js/gallerymac.js"></script> 

        <!-- //////////////////////////////////// -->
        <!-- //           Navigation Menu      // -->
        <!-- //////////////////////////////////// -->
        <script type="text/javascript">
            $(document).ready(function () {
                $("ul.menu_body li:even").addClass("alt");
                $('img.menu_head').click(function () {
                    $('ul.menu_body').slideToggle('medium');
                });
                $('ul.menu_body li a').mouseover(function () {
                    $(this).animate({ fontSize: "14px", paddingLeft: "20px" }, 50 );
                });
                $('ul.menu_body li a').mouseout(function () {
                    $(this).animate({ fontSize: "12px", paddingLeft: "10px" }, 50 );
                });
            });
        </script>

        <script type="text/javascript">
            $(function(){ 	
                //Tab Jquery
                $(".tab_content").hide(); 
                $("ul.tabs li:first").addClass("active").show(); 
                $(".tab_content:first").show(); 
                $("ul.tabs li").click(function() {
                    $("ul.tabs li").removeClass("active");
                    $(this).addClass("active"); 
                    $(".tab_content").hide(); 
                    var activeTab = $(this).find("a").attr("href"); 
                    $(activeTab).fadeIn(); 
                    return false;
                });
                $('#img-slide-large').cycle({
                    fx:     'fade',
                    speed:  'slow',
                    timeout: 0,
                    pager:  '#nav-thumb',
                    pagerAnchorBuilder: function(idx, slide) {
                        // return sel string for existing anchor
                        return '#nav-thumb li:eq(' + (idx) + ') a';
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                function filterPath(string) {
                    return string
                    .replace(/^\//,'')
                    .replace(/(index|default).[a-zA-Z]{3,4}$/,'')
                    .replace(/\/$/,'');
                }
                var locationPath = filterPath(location.pathname);
                var scrollElem = scrollableElement('html', 'body');

                $('a[href*=#]').each(function() {
                    var thisPath = filterPath(this.pathname) || locationPath;
                    if (  locationPath == thisPath
                        && (location.hostname == this.hostname || !this.hostname)
                        && this.hash.replace(/#/,'') ) {
                        var $target = $(this.hash), target = this.hash;
                        if (target) {
                            var targetOffset = $target.offset().top;
                            $(this).click(function(event) {
                                event.preventDefault();
                                $(scrollElem).animate({scrollTop: targetOffset}, 400, function() {
                                    location.hash = target;
                                });
                            });
                        }
                    }
                });

                // use the first element that is "scrollable"
                function scrollableElement(els) {
                    for (var i = 0, argLength = arguments.length; i <argLength; i++) {
                        var el = arguments[i],
                        $scrollElement = $(el);
                        if ($scrollElement.scrollTop()> 0) {
                            return el;
                        } else {
                            $scrollElement.scrollTop(1);
                            var isScrollable = $scrollElement.scrollTop()> 0;
                            $scrollElement.scrollTop(0);
                            if (isScrollable) {
                                return el;
                            }
                        }
                    }
                    return [];
                }

            });</script>

        <script type="text/javascript">
            $(function(){
                $('#slider').cycle({
                    timeout: 6000,  // milliseconds between slide transitions (0 to disable auto advance)
                    fx:     'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...            
                    prev:   '#slideprev', // selector for element to use as click trigger for next slide  
                    next:   '#slidenext', // selector for element to use as click trigger for previous slide
                    pause:   true,	  // true to enable "pause on hover"
                    cleartypeNoBg: true, // set to true to disable extra cleartype fixing (leave false to force background color setting on slides)
                    pauseOnPagerHover: 0 // true to pause when hovering over pager link
                });      
                $('.box-news-content ul').cycle({
                    timeout: 6000,  // milliseconds between slide transitions (0 to disable auto advance)
                    fx:     'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
                    pause:   0,	  // true to enable "pause on hover"
                    cleartypeNoBg: true, // set to true to disable extra cleartype fixing (leave false to force background color setting on slides)
                    pauseOnPagerHover: 0 // true to pause when hovering over pager link
                });
                $('.box-accesories ul').cycle({
                    timeout: 0,  // milliseconds between slide transitions (0 to disable auto advance)
                    fx:     'scrollHorz', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
                    prev:   '#accprev', // selector for element to use as click trigger for next slide  
                    next:   '#accnext', // selector for element to use as click trigger for previous slide
                    pause:   0,	  // true to enable "pause on hover"
                    cleartypeNoBg: true, // set to true to disable extra cleartype fixing (leave false to force background color setting on slides)
                    pauseOnPagerHover: 0 // true to pause when hovering over pager link
                });
            });
        </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37806403-2', 'skylinejaya.id');
  ga('send', 'pageview');

</script>
    </head>

    <body>	
        <div id="container">

            <div id="top-container">

                <!-- BEGIN OF HEADER -->
                <div id="header"> 

                    <!-- begin of logo -->
                    <div id="logo-wrapper">
                        <div id="logo"><a href="index.php"><img src="images/logo.png" alt="" /></a></div>
                    </div>
                    <!-- end of logo -->

                    <!-- begin of mainmenu -->
                    <div id="mainmenu">
                        <ul id="topnav">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About</a></li>
                            <li><a>Factories</a>
                                <div class="sub">
                                    <ul class="drop-text">
                                        <li>
                                            <p>PT. Skyline Jaya was officially established on 2005. Operates on 2 main factories with a vast total area of 10 hectares with approximately 1,000 employees.</p>
                                        </li>
                                    </ul> 
                                    <ul class="drop-link"> 
                                        <li><h2>Select Factory</h2></li>
                                        <li><a href="indoor-furniture.php">Indoor Furniture Plant</a></li>
                                        <li><a href="outdoor-furniture.php">Outdoor Furniture Plant</a></li> 
                                    </ul>                      
                                </div>
                            </li>
                            <!--<li><a href="location.php">Location</a></li> -->
                            <li><a href="career.php">Career</a></li> 
                            <li><a href="gallery.php">Gallery</a></li>                          
                            <li><a href="contact.php">Contact</a></li>                    
                        </ul>
                    </div> 
                    <!-- end of mainmenu -->

                </div>
                <!-- END OF HEADER --> 
