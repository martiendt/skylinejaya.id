<?php require_once 'header.php'; ?>

<!-- BEGIN OF SLIDESHOW -->
<div id="fullwidth-slider">            	
    <div class="slideshow-wrapper">           

        <ul id="slider">
            <li><!-- begin of slide 1 -->
                <div class="slide-content-wrapper">                            	
                    <div class="slide-content">
                        <div class="slide-img">
                            <img src="images/slide1-img.png" alt="" />
                        </div>
                        <div class="slide-text">    
                            <h2><a href="indoor-furniture.php" style="color:black;">Indoor Furniture Plant</a></h2>
                            <p>
                                Located at North East of Surabaya, 30 to 40 mins. by car from
                                Juanda International airport of Surabaya and 15 minutes by car to
                                Tanjung Perak International sea Port.
                                The factory occupies of 2.8 hectares land with +/- 2 hectare
                                covered area for production processing zone, considered as one
                                of the compliance vendor manufacturing Outdoor and Indoor
                                Furniture in Indonesia.
                            </p> 
                        </div>                                    
                    </div>
                    <div class="slide-dot"></div>
                </div>
                <img src="images/slideshow/slide1.jpg" alt="" />
            </li>
            <li><!-- begin of slide 2 -->
                <div class="slide-content-wrapper">                            	
                    <div class="slide-content">
                        <div class="slide-img">
                            <img src="images/slide1-img.png" alt="" />
                        </div>
                        <div class="slide-text"> 
                            <h2><a href="outdoor-furniture.php" style="color:black;">Outdoor Furniture Plant</a></h2>
                            <p>
                                Located at Sidoarjo, South West of Surabaya City, about  40 mins. by car from Juanda International airport of Surabaya. The factory occupies of 7.5 hectares land with +/- 6 hectare covered area for production processing zone, specializing in woven furniture, especially synthetic wicker.
                            </p> 
                        </div>
                    </div>
                    <div class="slide-dot"></div>
                </div>
                <img src="images/slideshow/slide2.jpg" alt="" />
            </li>           
        </ul>
        <div id="slide-nav">
            <div id="slideprev"></div>
            <div id="slidenext"></div>
        </div>                                         

    </div>                
    <div class="slide-flow"></div>
</div>     
<!-- END OF SLIDESHOW -->

<!-- BEGIN OF CONTENT -->
<div id="midbox-container">

    <div id="content">
        <div class="maincontent">

            <div class="front-content">
                <img src="images/front-image.jpg" alt="" class="imgleft" />
                <h3>Two Main Factories<br />
                    <span class="h-brown" style="font-size: 16px;">with a vast total area of 10 hectares with approximately 1,000 employees</span>                        
                </h3>
                <div class="front-column-left">
                    <p><strong><a href="indoor-furniture.php">INDOOR FURNITURE PLANT</a></strong><br />
                        Each stage of our production process is being filtered with a systematic Production Quality Station (PQS). PQS means a station where any defects is sorted out relevant to the process to be carried out in each particular spot where the PQS is located at.
                    </p>
                    <img src="images/indoor-fact.jpg" alt="" />
                </div>
                <div class="front-column-right">
                    <p><strong><a href="outdoor-furniture.php">OUTDOOR FURNITURE PLANT</a></strong><br />
                        Delivering you an environment that is not confined by walls, but defined by a sense of personal space, an oasis of peace, relaxation and freedom. View the open air as an extension of your home, an expression of your individual style. 
                    </p> 
                    <img src="images/outdoor-fact.jpg" alt="" style="margin-left: 20px;" />   
                </div>
            </div>

            <div id="bottom-box">
                <div class="bottom-content">
                    <div class="box-title2">
                        <h6>Need Support ?</h6>
                    </div>
                    <div class="box-content-bottom">
                        <a href="contact.php"><img src="images/cs.jpg" alt="" /></a>
                    </div>
                </div>

                <div class="bottom-content">
                    <div class="box-title2">
                        <h6>Product</h6>
                    </div>
                    <div class="box-accesories">
                        <ul>
                            <li class="acces-content">
                                <p>Product 1</p>
                                <img src="images/product1.jpg" alt="" />
                            </li> 
                            <li class="acces-content">
                                <p>Product 2</p>
                                <img src="images/product2.jpg" alt="" />
                            </li> 
                            <li class="acces-content">
                                <p>Product 3</p>
                                <img src="images/product3.jpg" alt="" />
                            </li> 
                            <li class="acces-content">
                                <p>Product 4</p>
                                <img src="images/product4.jpg" alt="" />
                            </li> 
                            <li class="acces-content">
                                <p>Product 5</p>
                                <img src="images/product5.jpg" alt="" />
                            </li> 
                        </ul>
                        <div id="accessories-nav">
                            <div id="accprev"></div>
                            <div id="accnext"></div>
                        </div>    
                    </div>
                </div>

                <div class="bottom-content-last">
                    <div class="box-title2">
                        <h6>Career</h6>
                    </div>
                    <div class="box-content-bottom">
                        <img src="images/community-icon.jpg" alt="" class="imgleft cmty-icon" />
                        <p>We welcome applications from Responsible individuals who possess a strong sense of Integrity and are constantly striving for Self-Renewal and Excellence.<br />
                            <a href="career.php"><strong>http://www.skylinejaya.id/career</strong></a></p>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<!-- END OF CONTENT -->

<?php require_once 'footer.php'; ?>
