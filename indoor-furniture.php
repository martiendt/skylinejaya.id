<?php require_once 'header.php'; ?>
<link href="css/gallerymac.css" rel="stylesheet" type="text/css" /> 
<link rel="stylesheet" href='css/hoverbox.css' type="text/css" media="screen, projection" />
<!--[if lte IE 7]>
<link rel="stylesheet" href='css/ie_fixes.css' type="text/css" media="screen, projection" />
<![endif]-->

<!--[if lte IE 7]>
<style type="text/css">
ul li{
	display:inline;
	/*float:left;*/
}
</style>
<![endif]-->

<!-- BEGIN OF CONTENT -->
<div id="midbox-container-inner">
    <div id="midbox-inner-bg"></div>
    <div id="midbox-inner">

        <!-- begin of page title -->
        <div id="page-title">
            <h1>Indoor Furniture Plant</h1>
        </div>
        <!-- end of page title -->

        <!-- begin of award content -->
        <div id="bread-search-column">
            <div id="breadcumb">                   	
                <span class="bread-img"><img src="images/home-icon.png" alt="" class="bread-img" /></span>
                <span class="bread-txt">&raquo; <a href="#about">Indoor Furniture Plant</a> | <a href="#production-process">Production Process</a> | <a href="#gallery">Gallery</a></span>
            </div>           
        </div>
        <!-- end of award content -->

    </div>

    <div id="content">
        <div id="menunav">
            <img src="images/navigate.png" width="184" height="32" class="menu_head" />
            <ul class="menu_body">
                <li><a href="#container">Main Menu</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#production-process">Production Process</a></li>
                <li><a href="#gallery">Gallery</a></li> 
            </ul>
        </div>
        <div id="content-left">  

            <div class="maincontent">

                <!-- begin of content-left -->

                <div id="about" style="width:631px">
                    <h3>About</h3>
                    <p>Located at North East of Surabaya, 30 to 40 mins. by car from
                        Juanda International airport of Surabaya and 15 minutes by car to
                        Tanjung Perak International sea port.
                        The factory occupies of 2.8 hectares land with +/- 2 hectare
                        covered area for production processing zone, considered as one
                        of the compliance vendor manufacturing Outdoor and Indoor
                        Furniture in Indonesia.
                        We have established our own Standard Operating Procedure,
                        continuously maintained by our in-house QA inspectors who are
                        well trained. Each stage of our production process is being
                        filtered with a systematic Production Quality Station (PQS). PQS
                        means a station where any defects is sorted out relevant to the
                        process to be carried out in each particular spot where the PQS
                        is located at, from incoming raw materials, milling, clamping,
                        veneering, assembly, upholstery, finishing and up to packaging
                        and loading process.
                        Apart from our own Standard Operating Procedure on
                        manufacturing wooden Indoor and Outdoor Furniture, we also
                        provide our QA inspectors with a Quality Assurance Manual Book
                        alongside with the provision of training in accordance to US and
                        European Indoor and Outdoor Manufacturing Standard.</p>
                    <img src="images/about-img-indoor.jpg" alt=""  /><br/>
<img src="images/about-img-indoor_2.jpg" alt=""  /><br/><br/><br/><br/>
                </div>         

                <div id="production-process">
                    <h3>Production Process</h3> 
                    <div id="gamKecil">
                        <ul class="hoverbox2"> 
                            <span>
                                <li>
                                    <a href="images/production-process/indoor-1.jpg"><img src="images/production-process/indoor-1.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/indoor-2.jpg"><img src="images/production-process/indoor-2.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/indoor-3.jpg"><img src="images/production-process/indoor-3.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/indoor-4.jpg"><img src="images/production-process/indoor-4.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/indoor-5.jpg"><img src="images/production-process/indoor-5.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/indoor-6.jpg"><img src="images/production-process/indoor-6.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/indoor-7.jpg"><img src="images/production-process/indoor-7.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/indoor-8.jpg"><img src="images/production-process/indoor-8.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                        </ul>

                        <ul class="display"><br/>
                            <li> 
                                <h6>1. In coming raw materials</h6> 
                            </li>  
                            <li> 
                                    <h6>2. Rough and fine milling</h6> 
                            </li>  
                            <li> 
                                    <h6>3. Clamping and Lamination</h6> 
                            </li>  
                            <li> 
                                    <h6>4. Final sanding and assembly</h6> 
                            </li>  
                            <li> 
                                    <h6>5. Veneering </h6> 
                            </li>  
                            <li> 
                                    <h6>6. Repairing Items - Finishing</h6> 
                            </li>  
                            <li> 
                                    <h6>7. 1 lines of 700 meters Conveyorized Finishing</h6> 
                            </li>  
                            <li> 
                                    <h6>8. Packing and Loading</h6> 
                            </li>  
                        </ul>   
                    </div>  
                </div>   
            </div> 


            <div id="content-left">
                <!-- begin of tab gallery -->
                <div id="gallery">
                    <h4 style="padding-left: 10px;padding-top: 10px;">Skyline Home Collection</h4>
                    <div id="slides">

                        <div class="slide"><img src="images/gallerymac/indoor/indoor1.jpg" width="635" height="300" alt="side" /></div> 
                        <div class="slide"><img src="images/gallerymac/indoor/indoor2.jpg" width="635" height="300" alt="side" /></div> 
                        <div class="slide"><img src="images/gallerymac/indoor/indoor3.jpg" width="635" height="300" alt="side" /></div> 
                        <div class="slide"><img src="images/gallerymac/indoor/indoor4.jpg" width="635" height="300" alt="side" /></div> 
                        <div class="slide"><img src="images/gallerymac/indoor/indoor5.jpg" width="635" height="300" alt="side" /></div> 
                        <div class="slide"><img src="images/gallerymac/indoor/indoor6.jpg" width="635" height="300" alt="side" /></div>  

                    </div>

                    <div id="menu">

                        <ul style="margin-left: 10px">
                            <li class="fbar">&nbsp;</li>
                            <li class="menuItem"><a href=""><img src="images/gallerymac/indoor/indoor1t.jpg" alt="thumbnail" /></a></li>
                            <li class="menuItem"><a href=""><img src="images/gallerymac/indoor/indoor2t.jpg" alt="thumbnail" /></a></li>
                            <li class="menuItem"><a href=""><img src="images/gallerymac/indoor/indoor3t.jpg" alt="thumbnail" /></a></li>
                            <li class="menuItem"><a href=""><img src="images/gallerymac/indoor/indoor4t.jpg" alt="thumbnail" /></a></li>
                            <li class="menuItem"><a href=""><img src="images/gallerymac/indoor/indoor5t.jpg" alt="thumbnail" /></a></li>
                            <li class="menuItem"><a href=""><img src="images/gallerymac/indoor/indoor6t.jpg" alt="thumbnail" /></a></li> 
                        </ul>
                    </div>                    
                </div>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <!-- end of tab gallery --> 
            </div>
            <!-- end of content-left --> 
        </div>
    </div>

</div>
<!-- END OF CONTENT -->
<?php require_once 'footer.php'; ?>