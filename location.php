<?php require_once 'header.php'; ?>  
<!-- //////////////////////////////////// -->
<!-- //Google Maps -7.249335,112.680684// -->
<!-- //////////////////////////////////// -->  
 <head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      html, body, #map-canvas {
        height: 100%;
        width:100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script>
var map;
function initialize() {
  var myLatlng = new google.maps.LatLng(-7.249335,112.680684);
  var mapOptions = {
    zoom: 14,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Hello World!'
  });
}

google.maps.event.addDomListener(window, 'load', initialize);


    </script>
  </head>
 
<div id="midbox-container-inner">
    <div id="midbox-inner-bg"></div>
    <div id="midbox-inner">

        <!-- begin of page title -->
        <div id="page-title">
            <h1>Location - Outdoor Furniture Plant</h1>
        </div>
        <!-- end of page title -->

        <!-- begin of award content -->
        <div id="bread-search-column">
            <div id="breadcumb">                    
                <span class="bread-img"><img src="images/home-icon.png" alt="" class="bread-img" /></span>
                <span class="bread-txt">&raquo; <a href="location.php">Indoor Furniture Plant<a/> | <a href="location-outdoor.php">Outdoor Furniture Plant<a/></span>
            </div>    
        </div>
        <!-- end of award content -->

    </div>

    <div id="content">
        <div class="maincontent" style="margin:0;padding:0">                  

            <!-- <div id="map-canvas"></div> -->
            <!-- begin of map --> 
            <div id="map-canvas" style="width: 960px; height: 400px;"></div> 
            <!-- end of map -->

            <!-- begin of content-left -->
            <div id="content-left">        

            </div>
            <!-- end of content-left -->


        </div>
    </div>

</div>


<!-- END OF CONTENT -->
<?php require_once 'footer.php'; ?>