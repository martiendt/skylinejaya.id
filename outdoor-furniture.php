<?php require_once 'header.php'; ?>
<link href="css/gallerymac.css" rel="stylesheet" type="text/css" /> 
<link rel="stylesheet" href='css/hoverbox.css' type="text/css" media="screen, projection" />
<!--[if lte IE 7]>
<link rel="stylesheet" href='css/ie_fixes.css' type="text/css" media="screen, projection" />
<![endif]-->

<!--[if lte IE 7]>
<style type="text/css">
ul li{
	display:inline;
	/*float:left;*/
}
</style>
<![endif]-->

<!-- BEGIN OF CONTENT -->
<div id="midbox-container-inner">
    <div id="midbox-inner-bg"></div>
    <div id="midbox-inner">

        <!-- begin of page title -->
        <div id="page-title">
            <h1>Outdoor Furniture Plant</h1>
        </div>
        <!-- end of page title -->

        <!-- begin of award content -->
        <div id="bread-search-column">
            <div id="breadcumb">                   	
                <span class="bread-img"><img src="images/home-icon.png" alt="" class="bread-img" /></span>
                <span class="bread-txt">&raquo; <a href="#about">Outdoor Furniture Plant</a> | <a href="#vision-mission">Vision &amp Mission</a> | <a href="#gallery">Gallery</a></span>
            </div>           
        </div>
        <!-- end of award content -->

    </div>

    <div id="content">
        <div id="menunav">
            <img src="images/navigate.png" width="184" height="32" class="menu_head" />
            <ul class="menu_body">
                <li><a href="#container">Main Menu</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#production-process">Production Process</a></li>
                <li><a href="#gallery">Gallery</a></li> 
            </ul>
        </div>
        <div id="content-left">  

            <div class="maincontent">

                <!-- begin of content-left -->

                <div id="about" style="width:631px">
                    <h3>About</h3>
                    <p>
                        Located at Sidoarjo, South West of Surabaya City, about  40 mins. by car from Juanda International airport of Surabaya. The factory occupies of 7.5 hectares land with +/- 6 hectare covered area for production processing zone, specializing in woven furniture, especially synthetic wicker.<br/><br/>
                        Woven furniture retains the classic style of traditional wicker pieces, but with the benefit of complete weather resistance. Because every piece is hand-woven, each item is completely unique. The frame are fully welded, powder coated aluminium, making the pieces light and highly resistant to corrosion. And made from only the highest quality fibre which has many benefits including, tear resistance, UV resistance and weather resistance. The wicker is also completely weatherproof, insensitive to differences in temperatureand extremely hard wearing.<br/><br/>
                        Delivering you an environment that is not confined by walls, but defined by a sense of personal space, an oasis of peace, relaxation and freedom. View the open air as an extension of your home, an expression of your individual style.
                    </p>
                    <img src="images/about-img-outdoor.jpg" alt=""  /><br/>
<img src="images/about-img-outdoor_2.jpg" alt=""  /><br/><br/><br/><br/>
                </div>         

                <div id="production-process">
                    <h3>Production Process</h3> 
                    <div id="gamKecil">


                        <ul class="display"><br/>
                            <li> 
                                <h6>1. Cutting - Punching - Jig Preparation</h6>                                
                            </li>                               
                        </ul>   
                        <ul class="hoverbox2"> 
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-1.1.jpg"><img src="images/production-process/outdoor-1.1.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-1.2.jpg"><img src="images/production-process/outdoor-1.2.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-1.3.jpg"><img src="images/production-process/outdoor-1.3.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-1.4.jpg"><img src="images/production-process/outdoor-1.4.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-1.5.jpg"><img src="images/production-process/outdoor-1.5.jpg" alt="description" /> </a>
                                </li>
                            </span>   
                        </ul>
                        <!---------------------------------->
                        <ul class="display"><br/>
                            <li> 
                                <h6>2. Rolling Materials</h6>                                
                            </li>                               
                        </ul>   
                        <ul class="hoverbox2"> 
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-2.1.jpg"><img src="images/production-process/outdoor-2.1.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-2.2.jpg"><img src="images/production-process/outdoor-2.2.jpg" alt="description" /> </a>
                                </li>
                            </span>   
                        </ul>
                        <!---------------------------------->
                        <ul class="display"><br/>
                            <li> 
                                <h6>3. Welding Frame</h6>                                
                            </li>                               
                        </ul>   
                        <ul class="hoverbox2"> 
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-3.1.jpg"><img src="images/production-process/outdoor-3.1.jpg" alt="description" /> </a>
                                </li>
                            </span>
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-3.2.jpg"><img src="images/production-process/outdoor-3.2.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-3.3.jpg"><img src="images/production-process/outdoor-3.3.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-3.4.jpg"><img src="images/production-process/outdoor-3.4.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-3.5.jpg"><img src="images/production-process/outdoor-3.5.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                        </ul>
                        <!---------------------------------->
                        <ul class="display"><br/>
                            <li> 
                                <h6>4. Leveling Frame</h6>                                
                            </li>                               
                        </ul>   
                        <ul class="hoverbox2"> 
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-4.1.jpg"><img src="images/production-process/outdoor-4.1.jpg" alt="description" /> </a>
                                </li>
                            </span>   
                        </ul>
                        <!---------------------------------->
                        <ul class="display"><br/>
                            <li> 
                                <h6>5. Grinding Frame</h6>                                
                            </li>                               
                        </ul>   
                        <ul class="hoverbox2"> 
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-5.1.jpg"><img src="images/production-process/outdoor-5.1.jpg" alt="description" /> </a>
                                </li>
                            </span>
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-5.2.jpg"><img src="images/production-process/outdoor-5.2.jpg" alt="description" /> </a>
                                </li>
                            </span>
                        </ul>
                        <!---------------------------------->
                        <ul class="display"><br/>
                            <li> 
                                <h6>6. QC Frame</h6>                                
                            </li>                               
                        </ul>   
                        <ul class="hoverbox2"> 
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-6.1.jpg"><img src="images/production-process/outdoor-6.1.jpg" alt="description" /> </a>
                                </li>
                            </span> 
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-6.2.jpg"><img src="images/production-process/outdoor-6.2.jpg" alt="description" /> </a>
                                </li>
                            </span> 
                        </ul>
                        <!---------------------------------->
                        <ul class="display"><br/>
                            <li> 
                                <h6>7. Wicker Preparation</h6>                                
                            </li>                               
                        </ul>   
                        <ul class="hoverbox2"> 
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-7.1.jpg"><img src="images/production-process/outdoor-7.1.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-7.2.jpg"><img src="images/production-process/outdoor-7.2.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-7.3.jpg"><img src="images/production-process/outdoor-7.3.jpg" alt="description" /> </a>
                                </li>
                            </span>  
                        </ul>
                        <!---------------------------------->
                        <ul class="display"><br/>
                            <li> 
                                <h6>8. Weaving</h6>                                
                            </li>                               
                        </ul>   
                        <ul class="hoverbox2"> 
                            <span>
                                <li>
                                    <a href="images/production-process/outdoor-8.1.jpg"><img src="images/production-process/outdoor-8.1.jpg" alt="description" /> </a>
                                </li>
                            </span>   
                        </ul>
                    </div>  
                </div>   
            </div> 


            <div id="content-left">
                <!-- begin of tab gallery -->
                <div id="gallery">
                    <h4 style="padding-left: 10px;padding-top: 10px;">Skyline Design Collection</h4>
                    <div id="slides">
                        <div class="slide"><img src="images/gallerymac/outdoor/outdoor1.jpg" width="635" height="300" alt="side" /></div>  
                        <div class="slide"><img src="images/gallerymac/outdoor/outdoor2.jpg" width="635" height="300" alt="side" /></div>  
                        <div class="slide"><img src="images/gallerymac/outdoor/outdoor3.jpg" width="635" height="300" alt="side" /></div>  
                        <div class="slide"><img src="images/gallerymac/outdoor/outdoor4.jpg" width="635" height="300" alt="side" /></div>  
                        <div class="slide"><img src="images/gallerymac/outdoor/outdoor5.jpg" width="635" height="300" alt="side" /></div>  
                        <div class="slide"><img src="images/gallerymac/outdoor/outdoor6.jpg" width="635" height="300" alt="side" /></div>  
                    </div>

                    <div id="menu">

                        <ul style="margin-left: 10px">
                            <li class="fbar">&nbsp;</li>
                            <li class="menuItem"><a href=""><img src="images/gallerymac/outdoor/outdoor1t.jpg" alt="thumbnail" /></a></li> 
                            <li class="menuItem"><a href=""><img src="images/gallerymac/outdoor/outdoor2t.jpg" alt="thumbnail" /></a></li> 
                            <li class="menuItem"><a href=""><img src="images/gallerymac/outdoor/outdoor3t.jpg" alt="thumbnail" /></a></li> 
                            <li class="menuItem"><a href=""><img src="images/gallerymac/outdoor/outdoor4t.jpg" alt="thumbnail" /></a></li> 
                            <li class="menuItem"><a href=""><img src="images/gallerymac/outdoor/outdoor5t.jpg" alt="thumbnail" /></a></li> 
                            <li class="menuItem"><a href=""><img src="images/gallerymac/outdoor/outdoor6t.jpg" alt="thumbnail" /></a></li>                             
                        </ul>
                    </div>                    
                </div>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <!-- end of tab gallery --> 
            </div>
        </div>

    </div>
    <!-- END OF CONTENT -->
    <?php require_once 'footer.php'; ?>